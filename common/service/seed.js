var config = require('../config/config.js');
var  lightwallet = require('../../dist/lightwallet.min.js');
var txutils = config.txutils;
var signing = config.signing;
var encryption = config.encryption;


function generate_seed() {
    var  new_seed = lightwallet.keystore.generateRandomSeed();
    console.info(new_seed);
}